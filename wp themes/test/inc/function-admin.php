<?php

/*

@package test-theme

	=============
	 Admin Page
	============
*/

function test_add_admin_page(){
	add_menu_page('Test theme options', 'Test', 'manage_options', 'test-theme', 'test_theme_create_page', get_template_directory_uri() . '/img/icon.png', 110);
}
add_action('admin_menu', 'test_add_admin_page');

function test_theme_create_page(){
	echo '<h1>Test Theme Options</h1>';
}
